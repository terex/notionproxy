package main

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/kjk/notionapi"
)

type PageResponse struct {
	PageUrl               string
	BlockRecords          []*notionapi.Record
	UserRecords           []*notionapi.Record
	CollectionRecords     []*notionapi.Record
	CollectionViewRecords []*notionapi.Record
	DiscussionRecords     []*notionapi.Record
	CommentRecords        []*notionapi.Record
}

type ErrorResponse struct {
	error string
}

func PageById(w http.ResponseWriter, r *http.Request) {
	client := &notionapi.Client{}

	w.Header().Set("Content-Type", "application/json")
	vars := mux.Vars(r)

	pageID := vars["pageID"]
	log.Printf("Page %s", pageID)
	page, err := client.DownloadPage(pageID)

	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	pageResponse := PageResponse{
		PageUrl:               page.NotionURL(),
		BlockRecords:          page.BlockRecords,
		UserRecords:           page.UserRecords,
		CollectionRecords:     page.CollectionRecords,
		CollectionViewRecords: page.CollectionViewRecords,
		DiscussionRecords:     page.DiscussionRecords,
		CommentRecords:        page.CommentRecords,
	}

	j, err := json.MarshalIndent(pageResponse, "", "  ")
	w.Write(j)
}

func main() {
	router := mux.NewRouter().StrictSlash(true)

	router.HandleFunc("/{pageID}", PageById)
	log.Fatal(http.ListenAndServe(":80", router))
}

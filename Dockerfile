FROM golang:1.16

ENV GO111MODULE=off \
    CGO_ENABLED=0 \
    GOOS=linux \
    GOARCH=amd64


COPY /notionproxy $GOPATH/src/notionproxy
WORKDIR $GOPATH/src/notionproxy
RUN go get -d -v 
RUN go build -o main .

EXPOSE 80
CMD ["/go/src/notionproxy/main"]
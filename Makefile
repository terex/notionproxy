build:
	docker build -t notion .

run: build
	docker run --rm -d -p "80:80" notion
